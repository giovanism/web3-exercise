const fs = require('fs')
const Web3 = require('web3');

/*
console.log('compiling...')

const source = fs.readFileSync('Signature.sol')
let input = {
  language: 'Solidity',
  sources: {
    'Signature.sol': {
      content: source.toString()
    }
  },
  settings: {
    outputSelection: {
      '*': {
        '*': ['*']
      }
    }
  }
}
const output = JSON.parse(solc.compile(JSON.stringify(input)))
const bytecode = output.contracts['Signature.sol'].Signature.bytecode
const abi = output.contracts['Signature.sol'].Signature.abi;

const newAccount = web3.eth.accounts.create();
{
  address: '0x528a985d7091D08fB4A470C2f8f3a249f467822d',
  privateKey: '0xf52b7dbd31c150558f1ffdbcf016c9af86b36fb72eb85370f37aa0f074802e35'
}
*/

(async() => {
  try {
    const abi = JSON.parse(fs.readFileSync('Signature.abi.json').toString())
    let web3 = new Web3('http://net.haratoken.app:8545')

    const address = '0x0795FDd4844EBB14E3ddF4aF072b2905b6EB046e'
    let contract = new web3.eth.Contract(abi, address)

    const account = '0x528a985d7091D08fB4A470C2f8f3a249f467822d'

    console.log(contract.defaultAccount)
    console.log(contract.defaultBlock)
    console.log(contract.defaultChain)
    console.log(contract.defaultCommon)
    console.log(contract.transactionBlockTimeout)
    console.log(contract.transactionConfirmationBlocks)
    console.log(contract.transactionPollingTimeout)

    contract.methods.addSignature('0x42', '0x1337').send({from: account}, (error, hash) => {
      console.log(`hash: ${hash}`)
    }).then((receipt) => {
      console.log(`receipt: ${receipt}`)
      contract.methods.getSignature('0x42').call({from: account})
      .then(result => {
        console.log(`result: ${result}`)
      })
      .catch(err => {
        console.log(`err call: ${err}`)
      })
    }).catch((err) => {
      console.log(`err send: ${err}`)
    })

  
    /*
    newContractInstance = await contract.send({
      from: accounts[0],
      gas: 1500000,
      gasPrice: '30000000000'
    })
    console.log(newContractInstance.options.address)
    */
  } catch (e) {
    console.log(e.message)
  }
})()