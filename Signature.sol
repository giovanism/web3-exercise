pragma solidity >=0.4.22 <0.7.0;

/**
 * @title Signature 
 * @dev Storing signature hasn
 */
contract Signature {

    mapping(bytes32=>bytes32) private signatures;
    
    // event for EVM logging
    event SignatureAdded(bytes32 indexed id, bytes32 signature, address indexed by);
    

    /**
     * @dev add signature
     * @param _id key of document
     * @param _signature hash signature of document
     */
    function addSignature(bytes32 _id, bytes32 _signature) public {
        signatures[_id] = _signature;
        emit SignatureAdded(_id, signatures[_id], msg.sender);
    }

    /**
     * @dev get id's signature
     * @return id's signature
     */
    function getSignature(bytes32 _id) external view returns (bytes32) {
        return signatures[_id];
    }
}
